FROM node:alpine

RUN npm install -g discord-irc

ENTRYPOINT ["discord-irc", "--config", "/etc/discord-irc/config.json"]
